package models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RegistroTest {

    @Test
    public void testGets(){
        Registro r = new Registro(1,2,"testNR",3,"testFR","testRR",
                new Bodega("nb","eb"));
        assertEquals("le codigo de Registro deberia ser: 1",r.getCodRegistro(),1);
        assertEquals("El codigo del producto deberia ser: 2",r.getCodProd(),2);
        assertEquals("El Nombre del producto deberia ser: testNR",r.getNombreProd(),"testNR");
        assertEquals("El Stock del producto deberia ser: 3",r.getStockProd(),3);
        assertEquals("La fecha de verificaiocn deberia ser: testFR",r.getFechaProd(),"testFR");
        assertEquals("La razon deberia ser: testRR",r.getRazon(),"testRR");
    }
}
