package models;

import org.junit.Test;
import static org.junit.Assert.*;
import models.*;

public class BodegaTest {

    @Test
    public void testGets(){
        Bodega b = new Bodega("SucursalTest", "EncargadoTest");
        assertEquals("El nombre de la sucursal deberia ser: SucursalTest", b.getNombreSucursal(), "SucursalTest");
        assertEquals("El Encargado de la suvursal deberia ser: EncargadoTest",b.getEncargado(),"EncargadoTest");
    }

    public void testSets(){
        Bodega b = new Bodega();
        b.setNombreSucursal("testSucursal");
        b.setEncargado("testEncargado");
        assertEquals("El nombre de la sucursal deberia ser: testSucursal",b.getNombreSucursal(),"testSucursal");
        assertEquals("El nombre del encargado deberia ser: testEncargado",b.getEncargado(),"testEncargado");
    }
}
